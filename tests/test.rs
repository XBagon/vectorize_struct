#[macro_use]
extern crate vectorize_struct;


#[derive(Debug,Vectorizable)]
struct Struct{
    i : i32,
    u : u32,
    b : bool,
    s : String,
    buui : (bool, u32, u8, i16),
    s2 : String,
}

#[test]
fn vectorize_test() {
    let s = Struct{
        i: -12,
        u: 61,
        b: false,
        s: String::from("asd"),
        buui: (true, 1, 5, -2),
        s2: String::from("fgh"),
    };

    for f in s.vectorize().iter(){
        use StructFieldsEnum::*;
        match f {
            s(x) | s2(x) => println!("{}",x),
            _ => {},
        }
    }
}

#[test]
fn vectorize_mut_test() {
    let mut s = Struct{
        i: -12,
        u: 61,
        b: false,
        s: String::from("asd"),
        buui: (true, 1, 5, -2),
        s2: String::from("fgh"),
    };

    for f in s.vectorize_mut().iter_mut() {
        use StructMutFieldsEnum::*;
        match f {
            i(x) => **x = -1,
            u(x) => **x = 1,
            b(x) => **x = true,
            s(x) | s2(x) => **x = String::from("DEFAULT"),
            buui(x) => **x = (false, 0, 0, 0),
        }
    }

    println!("{:?}",s);
}