extern crate proc_macro;
extern crate proc_macro2;
extern crate syn;
#[macro_use]
extern crate quote;

use syn::DeriveInput;
use proc_macro2::Span;

#[proc_macro_derive(Vectorizable)]
pub fn vectorizable(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ast : DeriveInput = syn::parse(input).unwrap();
    let result : proc_macro::TokenStream = match ast.data {
        syn::Data::Struct(ref d) => {
            let enum_tys = d.fields.iter().map(|f| {
                let ident = f.ident.as_ref().unwrap();
                let ty = &f.ty;
                quote!(#ident(&'a #ty))
            });
            let enum_mut_tys = d.fields.iter().map(|f| {
                let ident = f.ident.as_ref().unwrap();
                let ty = &f.ty;
                quote!(#ident(&'a mut #ty))
            });
            let vector_members = d.fields.iter().map(|f| {
                let ident = f.ident.as_ref().unwrap();
                quote!(#ident(&self.#ident))
            });
            let vector_mut_members = d.fields.iter().map(|f| {
                let ident = f.ident.as_ref().unwrap();
                quote!(#ident(&mut self.#ident))
            });
            let ident = &ast.ident;
            let mut enum_name = ast.ident.to_string();
            let mut enum_mut_name = enum_name.clone();
            enum_mut_name.push_str("MutFieldsEnum");
            enum_name.push_str("FieldsEnum");
            let enum_ident = proc_macro2::Ident::new(&enum_name, Span::call_site());
            let enum_mut_ident = proc_macro2::Ident::new(&enum_mut_name, Span::call_site());
            quote!(
                #[derive(Debug)]
                enum #enum_ident<'a> {
                    #(#enum_tys,)*
                }

                enum #enum_mut_ident<'a> {
                    #(#enum_mut_tys,)*
                }

                impl #ident {

                    fn vectorize(&self) -> Vec<#enum_ident> {
                        use #enum_ident::*;
                        vec![#(#vector_members,)*]
                    }

                    fn vectorize_mut(&mut self) -> Vec<#enum_mut_ident> {
                        use #enum_mut_ident::*;
                        vec![#(#vector_mut_members,)*]
                    }
                }
            )
        },
        syn::Data::Enum(_) => {panic!("Only for structs!")},
        syn::Data::Union(_) => {panic!("Only for structs!")},
    }.into();
    //panic!(result.to_string());
    result
}